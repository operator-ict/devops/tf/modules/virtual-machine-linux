resource "azurerm_resource_group" "rg" {
  name     = "rg-vm-linux-${var.name}"
  location = var.location
  tags     = var.tags
}

data "azurerm_client_config" "current" {}

module "network-security-rules" {
  source  = "gitlab.com/operator-ict/network-security-rules/azurerm"
  version = "0.1.0"

  name                                 = var.name
  rg_name                              = azurerm_resource_group.rg.name
  rg_location                          = azurerm_resource_group.rg.location
  default_destination_address_prefixes = tolist([azurerm_network_interface.nic.private_ip_address])
  security_rules                       = var.security_rules
}

resource "azurerm_network_interface_security_group_association" "public" {
  network_interface_id      = azurerm_network_interface.nic.id
  network_security_group_id = module.network-security-rules.nsg_id
}

data "azurerm_public_ip" "public" {
  count               = var.public_ip != null ? 1 : 0
  name                = var.public_ip
  resource_group_name = var.public_ip_resource_group
}

resource "azurerm_network_interface" "nic" {
  name                = "${var.name}-nic"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location

  ip_configuration {
    name                          = "internal"
    subnet_id                     = var.subnet_id
    private_ip_address_allocation = "Dynamic"

    # Only include the public IP configur]ation if the variable is set
    public_ip_address_id = var.public_ip != null ? data.azurerm_public_ip.public[0].id : null

  }
}

resource "tls_private_key" "ssh" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "azurerm_key_vault" "kv" {
  name                            = "vm-linux-${var.name}"
  location                        = azurerm_resource_group.rg.location
  resource_group_name             = azurerm_resource_group.rg.name
  sku_name                        = "standard"
  tenant_id                       = data.azurerm_client_config.current.tenant_id
  enabled_for_template_deployment = true
  enable_rbac_authorization       = true
  soft_delete_retention_days      = 30
  purge_protection_enabled        = false
  tags                            = var.tags
}

resource "azurerm_key_vault_secret" "ssh" {
  key_vault_id = azurerm_key_vault.kv.id
  name         = "ssh-key"
  value        = tls_private_key.ssh.private_key_pem
}

resource "azurerm_linux_virtual_machine" "vm" {
  name                = var.name
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  size                = var.sku
  admin_username      = var.admin_username

  network_interface_ids = [
    azurerm_network_interface.nic.id
  ]

  admin_ssh_key {
    username   = var.admin_username
    public_key = tls_private_key.ssh.public_key_openssh
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "StandardSSD_LRS"
    disk_size_gb         = "32"
  }

  source_image_reference {
    publisher = "canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts"
    version   = "latest"
  }
  dynamic "identity" {
    for_each = var.identity == null ? [] : ["X"]
    content {
      type         = var.identity.type
      identity_ids = try(var.identity.ids, [])
    }
  }
}

resource "azurerm_managed_disk" "data" {
  count                = var.existing_disk_id != "" ? 0 : 1
  name                 = "${var.name}-disk1"
  resource_group_name  = azurerm_resource_group.rg.name
  location             = azurerm_resource_group.rg.location
  storage_account_type = "StandardSSD_LRS"
  create_option        = "Empty"
  disk_size_gb         = var.data_disk_size
}

resource "azurerm_virtual_machine_data_disk_attachment" "data" {
  managed_disk_id    = var.existing_disk_id != "" ? var.existing_disk_id : azurerm_managed_disk.data[0].id
  virtual_machine_id = azurerm_linux_virtual_machine.vm.id
  lun                = "1"
  caching            = "ReadWrite"
}

resource "azurerm_backup_protected_vm" "vm" {
  count               = var.backup_vm ? 1 : 0
  resource_group_name = var.backup_rg
  recovery_vault_name = var.recovery_vault_name
  source_vm_id        = azurerm_linux_virtual_machine.vm.id
  backup_policy_id    = var.backup_policy_id[var.policy_name]
}
