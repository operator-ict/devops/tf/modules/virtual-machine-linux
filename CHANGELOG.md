# Unreleased
## Added
## Changed
## Fixed

# [0.1.9] - 2025-01-12
## Changed
- Add identity user assigned

# [0.1.8] - 2025-01-12
## Added
- Add identity

# [0.1.7] - 2024-07-02
## Changed
- Attach existing disk optionally - variable removed

# [0.1.6] - 2024-07-02
## Added
- Attach existing disk optionally

# [0.1.4]
## Cahnged
- Move nsg to separate module

# [0.1.3] - 2023-07-11
## Added
- Connect network security group and network interface

# [0.1.2] - 2023-06-19
## Added
- Add public IP as optional paramater
- Add firewall rules as optional paramater

# [0.1.1] - 2023-05-18
## Added
- Add variable for data disk size

# [0.1.0] - 2023-03-24
## Release notes
Managed disk a port 22 je hard coded, protoze není jasný další rozvoj a použití. 
## Added
- init
